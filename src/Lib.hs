module Lib
    ( someFunc
    ) where

import Data.Array
import Data.Function (on)
import Data.Foldable (maximumBy)
import Data.List (subsequences, groupBy)
import Control.Arrow

data Item = Item { name :: String
                 , weight :: Int
                 , value :: Int
                 }
  deriving (Show, Eq)

data Result = Result { weigthTotal :: Int
                     , valueTotal :: Int
                     , allItems :: [ Item ]
                     }
  deriving (Show, Eq)

-- | pantry is a list of all of our items
samples :: [Item]
samples = [ Item "map" 9 150
          , Item "compass" 13 35
          , Item "water" 153 200
          , Item "sandwitch" 50 160
          , Item "glucose" 15 60
          , Item "tin" 68 45
          , Item "banana" 27 60
          , Item "apple" 39 40
          , Item "cheese" 23 30
          , Item "beer" 52 10
          , Item "cream" 11 70
          , Item "camera" 32 30
          , Item "tshirt" 24 15
          , Item "trousers" 48 10
          , Item "umbrella" 73 40
          , Item "trousers" 42 70
          , Item "overclothes" 43 75
          , Item "notecase" 22 80
          , Item "sunglasses" 7 20
          , Item "towel" 18 12
          , Item "socks" 4 50
          , Item "book" 30 10
          ]

-- buggy !
ksack01 len maxWeight inputsL = dataStore ! (itemsN, maxWeight)
  where
    inputsL' = take len $ cycle inputsL
    inputs = listArray (1, len) inputsL'
    (i1, i2) = bounds inputs
    itemsN = i2 - i1 + 1
    solve 0 j = (0, [])
    solve i j
      | i <= 1 = (0, [])
      | weight (inputs ! i) > j = dataStore ! ((i-1), j)
      | otherwise = maximumBy (on compare fst) [ dataStore ! ((i-1), j)
                                               , add (i-1) j
                                               ]
    size = ((0,0), (itemsN, maxWeight))
    dataStore = listArray size [ solve i j | (i,j) <- range size]
    add i j = (v + value b, b:ls)
      where
        b = inputs ! (i+1)
        (!v, !ls)
          | j - weight b <= 0 = dataStore !(i, j)
          | otherwise = dataStore ! (i-1, j - weight b)

-- | slow brute force
slowKsack01 len maxWeight  = maximumBy (on compare fst) . fmap (\xs -> (total value xs, xs)) . candidates . mkInputs
  where
    mkInputs = take len . cycle
    candidates = filter ((<= maxWeight) . total weight) . subsequences
    total f = sum . fmap f

slowKsack01' len maxWeight inputsL = ds ! (len,maxWeight)
  where
    inputs = listArray (1,len) . take len . cycle $ inputsL
    size = ((0,0), (len,maxWeight))
    solve 0 _ = 0
    solve i j
      | wi > j = ds ! (i-1, j)
      | otherwise = max (ds ! (i-1, j)) (ds ! (i-1, j - wi) + vi)
      where
        vi = value (inputs ! i)
        wi = weight (inputs ! i)
    ds = listArray size [ solve i j | (i,j) <- range size]

-- faster
fastKsack01 len maxWeight inputsL = (id *** reverse) (ds ! (len,maxWeight))
  where
    inputs = listArray (1,len) . take len . cycle $ inputsL
    size = ((0,0), (len,maxWeight))
    solve 0 _ = (0,[])
    solve i j
      | wi > j = ds ! (i-1, j)
      | otherwise = maximumBy (on compare fst) [ds ! (i-1, j), (bv + vi, inp:bl)]
      where
        inp = inputs ! i
        vi = value inp
        wi = weight inp
        (bv,bl) = ds ! (i-1, j - wi)
    ds = listArray size [ solve i j | (i,j) <- range size]

someFunc :: IO ()
someFunc = do
  print ("fastKsack01", fastKsack01 10000 3000 samples)
  print ("slowKsack01", slowKsack01 27 300 samples)
